import fastapi
import restapi.router

app = fastapi.FastAPI()
restapi.router.configure(app)

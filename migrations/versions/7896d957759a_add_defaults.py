"""
add defaults.

Revision ID: 7896d957759a
Revises: 469be9889004
Create Date: 2023-04-30 21:40:59.457311

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "7896d957759a"
down_revision = "469be9889004"
branch_labels = None
depends_on = None


def upgrade() -> None:
    from sqlalchemy import insert
    from components.security import utils

    # Create root user with password 123456
    root_user = {
        "email": "root@example.com",
        "password": utils.hash_password("123456"),
        "first_name": "Root",
        "last_name": "User",
    }
    op.execute(
        insert(
            sa.table(
                "user",
                sa.column("email"),
                sa.column("password"),
                sa.column("first_name"),
                sa.column("last_name"),
            )  # type: ignore
        ).values(
            root_user
        )  # type: ignore
    )

    conn = op.get_bind()
    res = conn.execute(
        sa.select(sa.table("user", sa.column("id"))).filter(
            sa.column("email") == root_user["email"]  # type: ignore
        )
    )
    root_user_id = res.first()[0]  # type: ignore

    # Create list of permissions
    permissions = [
        {"name": "manage_orders"},
        {"name": "manage_customers"},
        {"name": "manage_inventory"},
        {"name": "manage_users"},
        {"name": "manage_products"},
        {"name": "manage_shipping"},
        {"name": "*"},
    ]

    op.bulk_insert(sa.table("permission", sa.column("name")), permissions)

    # Create role super admin that has permission "*"
    super_admin_role = {
        "name": "super_admin",
    }

    op.execute(
        insert(table=sa.table("role", sa.column("name")))
        .values(super_admin_role)
        .returning(sa.column("id"))  # type: ignore
    )
    res = conn.execute(
        sa.select(sa.table("role", sa.column("id"))).filter(
            sa.column("name") == super_admin_role["name"]  # type: ignore
        )
    )
    role_id = res.first()[0]  # type: ignore

    # Assign the '*' permission to the super admin role
    permission_id = conn.execute(
        sa.select(sa.table("permission", sa.column("id"))).where(
            sa.column("name") == "*"
        )  # type: ignore
    ).scalar()  # type: ignore

    op.execute(
        insert(
            sa.table(
                "role_permission", sa.column("role_id"), sa.column("permission_id")
            )
        ).values(
            {"role_id": role_id, "permission_id": permission_id}
        )  # type: ignore
    )

    # Assign role super admin to the root user
    op.execute(
        insert(
            sa.table("user_role", sa.column("user_id"), sa.column("role_id"))
        ).values(
            {"user_id": root_user_id, "role_id": role_id}
        )  # type: ignore
    )


def downgrade() -> None:
    pass

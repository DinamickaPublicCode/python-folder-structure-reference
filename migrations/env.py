from logging.config import fileConfig
from sqlalchemy import engine_from_config
from sqlalchemy import pool

from alembic import context
from components.core import database
from components.customer.models import *  # noqa: F403
from components.inventory.models import *  # noqa: F403
from components.order.models import *  # noqa: F403
from components.payment.models import *  # noqa: F403
from components.product.models import *  # noqa: F403
from components.shipping.models import *  # noqa: F403
from components.user.models import *  # noqa: F403


Base = database.Base

settings = database.Settings()
database_url = settings.get_sync_database_url("sqlite")


# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

config.set_main_option("sqlalchemy.url", database_url)


# Interpret the config file for Python logging.
# This line sets up loggers basically.
if config.config_file_name is not None:
    fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = Base.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def run_migrations_offline():
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url, target_metadata=target_metadata, literal_binds=True, compare_type=True
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    # this callback is used to prevent an auto-migration from being generated
    # when there are no changes to the database schema
    def process_revision_directives(context, revision, directives):
        if getattr(config.cmd_opts, "autogenerate", False):
            script = directives[0]
            if script.upgrade_ops.is_empty():
                directives[:] = []

    connectable = engine_from_config(
        config.get_section(config.config_ini_section),  # type: ignore
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )

    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=target_metadata,
            process_revision_directives=process_revision_directives,
            compare_type=True,
        )

        with context.begin_transaction():
            context.run_migrations()


# if we're running under Alembic, use the special environment
# test connection; otherwise, use regular SQLAlchemy connect()
# engines.
if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()

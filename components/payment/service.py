from typing import cast
import pydantic
from sqlalchemy import Column
import stripe
import stripe.error
from fastapi import Depends

from components.order import repository, models
from components.order.schemas import OrderUpdate
from components.payment import exceptions


class Settings(pydantic.BaseSettings):
    stripe_secret_key: str
    currency: str = "USD"


class PaymentService:
    def __init__(
        self, settings: Settings, orders_repository: repository.OrdersRepository
    ):
        self.orders_repository = orders_repository
        self.settings = settings

    @staticmethod
    def get(
        orders_repository: repository.OrdersRepository = Depends(
            repository.OrdersRepository.get
        ),
    ):
        return PaymentService(orders_repository=orders_repository, settings=Settings())

    async def make_payment(self, token: str, order: models.Order):
        try:
            stripe.Charge.create(
                amount=order.total,
                currency=self.settings.currency,
                source=token,
                description=f"Payment for Order #{order.id}",
            )
            order.status = "PAID"
            await self.orders_repository.update_order(
                cast(int, order.id), cast(OrderUpdate, order)
            )
        except stripe.error.CardError as e:
            raise exceptions.PaymentError(e.error.message)
        except stripe.error.StripeError:
            raise exceptions.PaymentServiceError("Error processing payment")

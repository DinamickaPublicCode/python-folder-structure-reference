from sqlalchemy import Column, DateTime, ForeignKey, Integer, Float
from components.core import database


class Payment(database.Base):
    __tablename__ = "payment"

    id = Column(Integer, primary_key=True)
    order_id = Column(Integer, ForeignKey("order.id"))
    amount = Column(Float)
    payment_date = Column(DateTime)

from typing import Optional

from pydantic import BaseModel


class ProductCreate(BaseModel):
    name: str
    description: str
    price: float
    sku: Optional[str] = None


class ProductItem(BaseModel):
    id: int
    name: str
    description: str
    price: float
    sku: Optional[str] = None

    class Config:
        orm_mode = True

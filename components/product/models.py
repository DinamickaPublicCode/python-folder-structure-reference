from typing import Any
from sqlalchemy import Column, ForeignKey, Integer, String, Float
from components.core import database

from sqlalchemy.orm import relationship


class Category(database.Base):
    __tablename__ = "category"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    products: list[Any] = relationship("Product", back_populates="category")


class Product(database.Base):
    __tablename__ = "product"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)
    sku = Column(String)
    price = Column(Float)
    category_id = Column(Integer, ForeignKey("category.id"), nullable=True)
    category: Category = relationship("Category", back_populates="products")

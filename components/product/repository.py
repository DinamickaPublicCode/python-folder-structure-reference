from typing import cast

from fastapi import Depends
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from components.core import database
from components.product.models import Product
from components.product.schemas import ProductCreate


class ProductsRepository:
    def __init__(self, db: AsyncSession):
        self.db = db

    async def create_product(self, product: ProductCreate) -> Product:
        db_product = Product(**product.dict())
        self.db.add(db_product)
        await self.db.commit()
        await self.db.refresh(db_product)
        return db_product

    async def get_products(self) -> list[Product]:
        products = await self.db.execute(select(Product))
        return cast(list[Product], products.scalars().all())

    @staticmethod
    async def get(db: AsyncSession = Depends(database.get)):
        return ProductsRepository(db)

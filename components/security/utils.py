import passlib.context
import pydantic
from datetime import datetime, timedelta
import jose.jwt


class Settings(pydantic.BaseSettings):
    jwt_secret_key: str = (
        "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
    )
    jwt_algorithm: str = "HS256"
    jwt_token_expires_minutes = 30

    @staticmethod
    def get():
        return Settings()  # type: ignore


def create_access_token(data: dict):
    settings = Settings()
    expires_delta = timedelta(minutes=settings.jwt_token_expires_minutes)

    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jose.jwt.encode(
        to_encode, settings.jwt_secret_key, algorithm=settings.jwt_algorithm
    )
    return encoded_jwt


def decode_jwt_token(token: str):
    try:
        settings = Settings()
        payload: dict[str, str] | None = jose.jwt.decode(
            token, settings.jwt_secret_key, algorithms=[settings.jwt_algorithm]
        )
        if not payload:
            return None

        return payload.get("sub")
    except jose.JWTError:
        return None


pwd_context = passlib.context.CryptContext(schemes=["bcrypt"], deprecated="auto")


def hash_password(password: str) -> str:
    return pwd_context.hash(password)


def verify_password(plain_password: str, hashed_password: str) -> bool:
    return pwd_context.verify(plain_password, hashed_password)

import pydantic


class TokenData(pydantic.BaseModel):
    user_id: str

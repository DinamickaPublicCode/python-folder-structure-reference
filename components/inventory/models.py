from sqlalchemy import Column, ForeignKey, Integer
from components.core import database


class Inventory(database.Base):
    __tablename__ = "inventory"

    id = Column(Integer, primary_key=True)
    product_id = Column(Integer, ForeignKey("product.id"))
    quantity = Column(Integer)

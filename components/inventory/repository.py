from typing import Optional, cast

from fastapi import Depends
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from components.inventory.schemas import InventoryCreate, InventoryUpdate
from components.core import database
from components.inventory.models import Inventory


class InventoryRepository:
    def __init__(self, db: AsyncSession):
        self.db = db

    async def create_inventory(self, inventory: InventoryCreate) -> Inventory:
        db_inventory = Inventory(**inventory.dict())
        self.db.add(db_inventory)
        await self.db.commit()
        await self.db.refresh(db_inventory)
        return db_inventory

    async def update_inventory(
        self, inventory_id: int, inventory: InventoryUpdate
    ) -> Optional[Inventory]:
        db_inventory = await self.get_inventory_by_id(inventory_id)
        if db_inventory is None:
            return None
        for field, value in inventory.dict(exclude_unset=True).items():
            setattr(db_inventory, field, value)
        await self.db.commit()
        await self.db.refresh(db_inventory)
        return db_inventory

    async def get_inventory_by_id(self, inventory_id: int) -> Optional[Inventory]:
        query = select(Inventory).where(Inventory.id == inventory_id)
        result = await self.db.execute(query)
        return result.scalar_one_or_none()

    async def get_inventories(self) -> list[Inventory]:
        inventories = await self.db.execute(select(Inventory))
        return cast(list[Inventory], inventories.scalars().all())

    async def get_inventory_by_product_id(self, product_id: int) -> Optional[Inventory]:
        query = select(Inventory).where(Inventory.product_id == product_id)
        result = await self.db.execute(query)
        return result.scalar_one_or_none()

    @staticmethod
    async def get(db: AsyncSession = Depends(database.get)):
        return InventoryRepository(db)

from pydantic import BaseModel


class InventoryCreate(BaseModel):
    product_id: int
    quantity: int


class InventoryUpdate(BaseModel):
    quantity: int


class InventoryItem(BaseModel):
    id: int
    product_id: int
    quantity: int

    class Config:
        orm_mode = True

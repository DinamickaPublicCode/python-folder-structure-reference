from sqlalchemy import Column, ForeignKey, Integer, String
from components.core import database


class Shipping(database.Base):
    __tablename__ = "shipping"

    id = Column(Integer, primary_key=True)
    order_id = Column(Integer, ForeignKey("order.id"))
    address = Column(String)
    city = Column(String)
    state = Column(String)
    country = Column(String)
    postal_code = Column(String)

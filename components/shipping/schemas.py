from typing import Optional

from pydantic import BaseModel


class ShippingCreate(BaseModel):
    order_id: int
    address: str
    city: str
    state: str
    country: str
    postal_code: str


class ShippingUpdate(BaseModel):
    address: Optional[str]
    city: Optional[str]
    state: Optional[str]
    country: Optional[str]
    postal_code: Optional[str]


class ShippingItem(BaseModel):
    id: int
    order_id: int
    address: str
    city: str
    state: str
    country: str
    postal_code: str

    class Config:
        orm_mode = True

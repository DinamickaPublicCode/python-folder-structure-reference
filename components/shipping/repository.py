from typing import Optional, cast

from fastapi import Depends
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from components.core import database
from components.shipping.models import Shipping
from components.shipping.schemas import ShippingCreate, ShippingUpdate


class ShippingRepository:
    def __init__(self, db: AsyncSession):
        self.db = db

    async def create_shipment(self, shipping: ShippingCreate) -> Shipping:
        db_shipping = Shipping(**shipping.dict())
        self.db.add(db_shipping)
        await self.db.commit()
        await self.db.refresh(db_shipping)
        return db_shipping

    async def update_shipment(
        self, shipping_id: int, shipping: ShippingUpdate
    ) -> Optional[Shipping]:
        db_shipping = await self.get_shipment(shipping_id)
        if db_shipping is None:
            return None
        for field, value in shipping.dict(exclude_unset=True).items():
            setattr(db_shipping, field, value)
        await self.db.commit()
        await self.db.refresh(db_shipping)
        return db_shipping

    async def get_shipment(self, shipping_id: int) -> Optional[Shipping]:
        query = select(Shipping).where(Shipping.id == shipping_id)
        result = await self.db.execute(query)
        return result.scalar_one_or_none()

    async def get_shipments_for_order(self, order_id: int) -> list[Shipping]:
        query = select(Shipping).where(Shipping.order_id == order_id)
        result = await self.db.execute(query)
        return cast(list[Shipping], result.scalars().all())

    @staticmethod
    async def get(db: AsyncSession = Depends(database.get)):
        return ShippingRepository(db)

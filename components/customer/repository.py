from typing import cast, Optional

from fastapi import Depends
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from components.core import database
from components.customer.models import Customer
from components.customer.schemas import CustomerCreate, CustomerUpdate


class CustomersRepository:
    def __init__(self, db: AsyncSession):
        self.db = db

    async def create_customer(self, customer: CustomerCreate) -> Customer:
        db_customer = Customer(**customer.dict())
        self.db.add(db_customer)
        await self.db.commit()
        await self.db.refresh(db_customer)
        return db_customer

    async def get_customers(self) -> list[Customer]:
        customers = await self.db.execute(select(Customer))
        return cast(list[Customer], customers.scalars().all())

    async def update_customer(
        self, customer_id: int, customer: CustomerUpdate
    ) -> Optional[Customer]:
        db_customer = await self.db.get(Customer, customer_id)
        if db_customer is None:
            return None
        for field, value in customer.dict(exclude_unset=True).items():
            setattr(db_customer, field, value)
        await self.db.commit()
        await self.db.refresh(db_customer)
        return cast(Customer | None, db_customer)

    async def delete_customer(self, customer_id: int) -> Optional[Customer]:
        db_customer = await self.db.get(Customer, customer_id)
        if db_customer is None:
            return None
        await self.db.delete(db_customer)
        await self.db.commit()

        return cast(Customer | None, db_customer)

    async def get_customer_by_email(self, email: str) -> Optional[Customer]:
        query = select(Customer).where(Customer.email == email)
        result = await self.db.execute(query)
        return result.scalar_one_or_none()

    @staticmethod
    async def get(db: AsyncSession = Depends(database.get)):
        return CustomersRepository(db)

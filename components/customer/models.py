from sqlalchemy import Column, Integer, String
from components.core import database


class Customer(database.Base):
    __tablename__ = "customer"

    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    email = Column(String)
    password = Column(String)

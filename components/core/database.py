"""
Setup database configuration
"""
from typing import Any, Optional, cast
import pydantic
from sqlalchemy.ext.asyncio import AsyncSession, AsyncEngine
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import sessionmaker, declarative_base

Base = declarative_base()


class Settings(pydantic.BaseSettings):
    database_url: str = "sqlite+aiosqlite:///./mydatabase.db"

    def get_sync_database_url(self, protocol_name: str | None = None):
        """
        Helper function to allow sync engine to be initiated
        """
        parts = self.database_url.split("://", 1)
        return f"{protocol_name or parts[0]}://{parts[1]}"


class DatabaseManager:
    def __init__(self, engine: AsyncEngine) -> None:
        self.engine = engine

    def get_session(self):
        return sessionmaker(
            cast(Any, self.engine), class_=AsyncSession, expire_on_commit=False
        )

    instance: Optional[Any] = None

    @staticmethod
    def get():
        if not DatabaseManager.instance:
            settings = Settings()
            engine = create_async_engine(settings.database_url, echo=True)
            DatabaseManager.instance = DatabaseManager(engine)

        return cast(DatabaseManager, DatabaseManager.instance)


async def get():
    async_session = DatabaseManager.get().get_session()
    async with async_session() as session:
        yield cast(AsyncSession, session)

from typing import Optional, List

import pydantic
from pydantic import BaseModel


class Role(pydantic.BaseModel):
    id: int
    name: str


class User(pydantic.BaseModel):
    id: int
    role: Role = Role(name="guest", id=0)
    permissions: list[str] = []
    email: Optional[str] = None
    first_name: Optional[str] = None
    last_name: Optional[str] = None


class UserCreate(BaseModel):
    email: str
    password: str
    first_name: str
    last_name: str


class UserUpdate(BaseModel):
    email: Optional[str]
    password: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]


class UserPermissionUpdate(BaseModel):
    name: str


class UserRoles(BaseModel):
    roles: list[int]


class PasswordReset(BaseModel):
    email: str
    new_password: str


class PermissionItem(BaseModel):
    id = int
    name: str

    class Config:
        orm_mode = True


class RoleItem(BaseModel):
    id: int
    name: str
    permissions: Optional[List[PermissionItem]] = []

    class Config:
        orm_mode = True


class UserSchema(BaseModel):
    id: int
    email: str
    first_name: str
    last_name: str

    class Config:
        orm_mode = True


class UserFull(UserSchema):
    roles: list[RoleItem]


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: str | None = None

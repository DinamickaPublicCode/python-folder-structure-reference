from typing import cast

import fastapi

# from api.routes.user import User
from components.security import utils
from components.user import models
from components.user import repository


class AuthService:
    def __init__(self, user_repo: repository.UserRepository) -> None:
        self.user_repo = user_repo

    @staticmethod
    def get(
        user_repo: repository.UserRepository = fastapi.Depends(
            repository.UserRepository.get
        ),
    ):
        return AuthService(user_repo=user_repo)

    async def get_user_by_email(self, email: str):
        return await self.user_repo.get_user_by_email(email)

    async def authenticate_user(self, email: str, password: str) -> models.User | None:
        user = await self.user_repo.get_user_by_email(email)
        if not user:
            return None

        if not utils.verify_password(password, cast(str, user.password)):
            return None
        return user

from typing import cast, List, Optional

from fastapi import Depends
from sqlalchemy import select

from components.core import database, exceptions
from components.security import utils
from components.user import models
from components.user.models import Role, Permission


class UserRepository:
    def __init__(self, db: database.AsyncSession):
        self.db = db

    async def get_user_by_email(self, email: str) -> models.User | None:
        query = select(models.User).filter(models.User.email == email)
        result = await self.db.execute(query)
        return result.scalar_one_or_none()

    async def get_user_by_id(self, user_id: int) -> models.User | None:
        query = select(models.User).filter(models.User.id == user_id)
        result = await self.db.execute(query)
        return result.scalar_one_or_none()

    async def create_user(self, user: models.User) -> models.User:
        existing_user = await self.get_user_by_email(cast(str, user.email))
        if existing_user:
            raise exceptions.ItemAlreadyExists()

        user.password = utils.hash_password(cast(str, user.password))
        self.db.add(user)
        await self.db.commit()
        return user

    async def update_user_by_id(
        self, user_id: int, data: dict | None = None
    ) -> models.User | None:
        user = await self.get_user_by_id(user_id)
        if data:
            for key, value in data.items():
                setattr(user, key, value)

        await self.db.commit()
        await self.db.refresh(user)
        return user

    async def update_user(
        self, user: models.User, data: dict | None = None
    ) -> models.User:
        if data:
            for key, value in data.items():
                setattr(user, key, value)
        await self.db.commit()
        await self.db.refresh(user)
        return user

    async def add_user_permission(
        self, user_id: int, permission: Permission
    ) -> models.User:
        user = await self.get_user_by_id(user_id)
        if not user:
            raise exceptions.ItemNotFoundException()

        user.permissions.append(permission)
        await self.db.commit()
        await self.db.refresh(user)
        return user

    async def get_user_roles(self, user_id: int) -> List[Role]:
        user = await self.get_user_by_id(user_id)
        if not user:
            raise exceptions.ItemNotFoundException()
        return user.roles

    async def add_user_role(self, user_id: int, role: Role) -> models.User:
        user = await self.get_user_by_id(user_id)
        if not user:
            raise exceptions.ItemNotFoundException()
        user.roles.append(role)
        await self.db.commit()
        await self.db.refresh(user)
        return user

    @staticmethod
    async def get(db: database.AsyncSession = Depends(database.get)):
        return UserRepository(db=db)

    async def get_permission_by_name(self, name: str) -> Optional[Permission]:
        query = select(Permission).where(Permission.name == name)
        result = await self.db.execute(query)
        return result.scalar_one_or_none()

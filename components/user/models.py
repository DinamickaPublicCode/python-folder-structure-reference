from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from components.core import database


class Permission(database.Base):
    __tablename__ = "permission"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)


class Role(database.Base):
    __tablename__ = "role"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    permissions: list[Permission] = relationship(
        "Permission", secondary="role_permission", lazy="subquery"
    )


class UserRole(database.Base):
    __tablename__ = "user_role"

    user_id = Column(Integer, ForeignKey("user.id"), primary_key=True)
    role_id = Column(Integer, ForeignKey("role.id"), primary_key=True)


class User(database.Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    email = Column(String, unique=True)
    password = Column(String)
    first_name = Column(String)
    last_name = Column(String)
    roles: list[Role] = relationship("Role", secondary="user_role", lazy="subquery")
    permissions: list[Permission] = relationship(
        "Permission", secondary="user_permission", lazy="subquery"
    )


class RolePermission(database.Base):
    __tablename__ = "role_permission"

    role_id = Column(Integer, ForeignKey("role.id"), primary_key=True)
    permission_id = Column(Integer, ForeignKey("permission.id"), primary_key=True)


class UserPermission(database.Base):
    __tablename__ = "user_permission"

    user_id = Column(Integer, ForeignKey("user.id"), primary_key=True)
    permission_id = Column(Integer, ForeignKey("permission.id"), primary_key=True)

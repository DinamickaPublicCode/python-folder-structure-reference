from typing import Optional

from pydantic import BaseModel


class OrderCreate(BaseModel):
    customer_id: int
    total: float


class OrderItemCreate(BaseModel):
    order_id: int
    product_id: int
    quantity: int
    price: float


class OrderUpdate(BaseModel):
    status: Optional[str]
    total: Optional[float]


class OrderItemUpdate(BaseModel):
    quantity: Optional[int]
    price: Optional[float]


class OrderItemItem(BaseModel):
    id: int
    order_id: int
    product_id: int
    quantity: int
    price: float

    class Config:
        orm_mode = True


class OrderItemSchema(BaseModel):
    id: int
    order_date: str
    status: str
    total: float
    customer_id: int
    items: list[OrderItemItem]

    class Config:
        orm_mode = True

"""Repository for accessing Order objects in the database."""
from datetime import datetime
from typing import cast, Optional

from fastapi import Depends
from sqlalchemy import select, ColumnElement
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from components.core import database
from components.order.models import Order
from components.order.schemas import (
    OrderCreate,
    OrderItemCreate,
    OrderItemSchema,
    OrderUpdate,
    OrderItemUpdate,
)


class OrdersRepository:
    def __init__(self, db: AsyncSession) -> None:
        self.db = db

    async def create_order(self, order: OrderCreate) -> Order:
        db_order = Order(**order.dict())
        db_order.order_date = datetime.now()

        self.db.add(db_order)
        await self.db.commit()
        await self.db.refresh(db_order)
        return db_order

    async def create_order_item(self, order_item: OrderItemCreate) -> OrderItemSchema:
        db_order_item = OrderItemSchema(**order_item.dict())
        self.db.add(db_order_item)
        await self.db.commit()
        await self.db.refresh(db_order_item)
        return db_order_item

    async def update_order(self, order_id: int, order: OrderUpdate) -> Optional[Order]:
        db_order = await self.get_order(order_id)
        if db_order is None:
            return None
        for field, value in order.dict(exclude_unset=True).items():
            setattr(db_order, field, value)
        await self.db.commit()
        await self.db.refresh(db_order)
        return db_order

    async def update_order_item(
        self, order_item_id: int, order_item: OrderItemUpdate
    ) -> Optional[OrderItemSchema]:
        db_order_item = await self.get_order_item(order_item_id)
        if db_order_item is None:
            return None
        for field, value in order_item.dict(exclude_unset=True).items():
            setattr(db_order_item, field, value)
        await self.db.commit()
        await self.db.refresh(db_order_item)
        return db_order_item

    async def get_order(self, order_id: int) -> Optional[Order]:
        query = (
            select(Order).where(Order.id == order_id).options(joinedload(Order.items))
        )
        result = await self.db.execute(query)
        return result.scalar_one_or_none()

    async def get_orders(self) -> list[Order]:
        orders = await self.db.execute(select(Order).options(joinedload(Order.items)))
        return cast(list[Order], orders.scalars().all())

    async def get_order_item(self, order_item_id: int) -> Optional[OrderItemSchema]:
        query = select(OrderItemSchema).where(
            cast(ColumnElement[bool], OrderItemSchema.id == order_item_id)
        )
        result = await self.db.execute(query)
        return result.scalar_one_or_none()

    @staticmethod
    async def get(db: AsyncSession = Depends(database.get)):
        return OrdersRepository(db)

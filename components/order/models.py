from typing import Any
from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, Float
from sqlalchemy.orm import relationship

from components.core import database
from components.customer.models import Customer


class OrderItem(database.Base):
    __tablename__ = "order_item"

    id = Column(Integer, primary_key=True)
    order_id = Column(Integer, ForeignKey("order.id"))
    product_id = Column(Integer, ForeignKey("product.id"))
    quantity = Column(Integer)
    price = Column(Float)
    order: Any = relationship("Order", back_populates="items")


class Order(database.Base):
    __tablename__ = "order"

    id = Column(Integer, primary_key=True)
    order_date = Column(DateTime)
    status = Column(String)
    total = Column(Float)
    customer_id = Column(Integer, ForeignKey("customer.id"))
    customer: Customer = relationship("Customer")
    items: list[OrderItem] = relationship("OrderItem", back_populates="order")

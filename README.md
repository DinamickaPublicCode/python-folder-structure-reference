# Python-based backend

# NOTE: This example is not UP TO DATE. It's Work in progress. But it's good enough to reference the folder structure for your project

This is a basic ecommerce backend with example roles/JWT auth/other ecommerce entity management.

## Python version of choice 3.10

We use version **3.10** (for now) to be compatible with the most of python versions. When the right time comes, we'll migrate to 3.11 or later

## Package management

### Poetry

Packages are defined with poetry. Poetry is the most performant and deterministic way to manage dependencies in comparison to something like pipenv
or just virtual environments.

## Project setup

### `main.py` — entry point for the application at the root of the project

If the package is an executable then on top level we add a `main.py` that executes an application.

**Note: ** top-level directory isn't a python package itself. Do not add **init**.py at top level.

import fastapi

from restapi.routes import product, customer, payment, inventory, order, shipping, user


def configure(app: fastapi.FastAPI):
    router = fastapi.APIRouter(prefix="/api")

    router.include_router(customer.router)
    router.include_router(inventory.router)
    router.include_router(order.router)
    router.include_router(product.router)
    router.include_router(payment.router)
    router.include_router(shipping.router)
    router.include_router(user.router)

    return app.include_router(router)

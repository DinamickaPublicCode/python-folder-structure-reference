"""Defines helper functions for REST API user/permission validation."""
import functools
from typing import Annotated, Callable

import fastapi
import fastapi.security
from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy import select
from sqlalchemy.orm import selectinload
from starlette import status

from restapi import permissions
from components.core import database
from components.security import utils
from components.user import models as user_models
from components.user.repository import UserRepository

bearer = fastapi.security.HTTPBearer()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="api/users/token")


async def get_current_user(
    token: Annotated[str, Depends(oauth2_scheme)],
    user_repo: UserRepository = fastapi.Depends(UserRepository.get),
) -> user_models.User:
    """
    Get current user based on the JWT token provided in the header.

    Args:
        token (Annotated[str, Depends(oauth2_scheme)]): JWT token
        user_repo (UserRepository, optional): User repository.
        Defaults to Depends(UserRepository.get).

    Raises:
        HTTPException: Exception is raised if the credentials cannot be validated.

    Returns:
        user_models.User: User object corresponding to the authenticated user
    """
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    email = utils.decode_jwt_token(token)
    if email is None:
        raise credentials_exception

    user = await user_repo.get_user_by_email(email=email)
    if user is None:
        raise credentials_exception
    return user


def requires_permission(*kwargs: list[str]) -> Callable:
    """
    Decorator function to check if the user has the required permissions
    to perform an action.

    Args:
        kwargs: Required permissions

    Raises:
        HTTPException: Exception is raised if the user does not have the
        required permissions to perform an action.

    Returns:
        callable: Function to be decorated
    """

    async def wrapper(
        db: database.AsyncSession = fastapi.Depends(database.get),
        user: user_models.User = fastapi.Depends(get_current_user),
    ) -> None:
        """
        Wrapper function for the decorated function to check user permissions.

        Args:
            db (database.AsyncSession, optional): Database session.
            Defaults to Depends(database.get).
            user (user_models.User, optional): User object.
            Defaults to Depends(get_current_user).

        Raises:
            HTTPException: Exception is raised if the user does not have
            the required permissions to perform an action.

        Returns:
            None
        """
        filtered_users = select(user_models.User).filter(user_models.User.id == user.id)
        filtered_users.options(
            selectinload(user_models.User.roles, user_models.Role.permissions)
        )
        user_roles = (await db.execute(filtered_users)).scalar_one().roles

        all_permissions = set(
            permission.name
            for permission in (
                *user.permissions,
                *(
                    functools.reduce(
                        lambda previous, current: list(set(*previous, *current)),
                        map(lambda user_role: user_role.permissions, user_roles),
                    )
                    if user_roles
                    else ()
                ),
            )
        )

        if permissions.Permissions.ALL not in all_permissions:
            if not set(kwargs).issubset(all_permissions):
                raise fastapi.HTTPException(
                    status_code=fastapi.status.HTTP_403_FORBIDDEN,
                    detail="Not authorized to perform this action",
                )
        # return await func(*kwargs)

    return wrapper

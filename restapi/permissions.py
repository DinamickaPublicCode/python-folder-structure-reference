import enum


class Permissions(str, enum.Enum):
    MANAGE_ORDERS = "manage_orders"
    MANAGE_CUSTOMERS = "manage_customers"
    MANAGE_INVENTORY = "manage_inventory"
    MANAGE_USERS = "manage_users"
    MANAGE_PRODUCTS = "manage_products"
    MANAGE_SHIPPING = "manage_shipping"
    ALL = "*"

import fastapi
from fastapi import APIRouter, Depends

from components.product.repository import ProductsRepository
from components.product.schemas import ProductCreate, ProductItem

router = APIRouter(prefix="/products", tags=["Products"])


@router.post(
    "/", response_model=ProductItem, status_code=fastapi.status.HTTP_201_CREATED
)
async def create_product(
    product: ProductCreate,
    products_repo: ProductsRepository = Depends(ProductsRepository.get),
):
    db_product = await products_repo.create_product(product)
    return ProductItem.from_orm(db_product)


@router.get("/", response_model=list[ProductItem])
async def get_products(
    products_repo: ProductsRepository = Depends(ProductsRepository.get),
):
    products = await products_repo.get_products()
    return list(map(lambda x: ProductItem.from_orm(x), products))

from fastapi import APIRouter, Depends, HTTPException, status

from restapi.security import get_current_user
from components.order import repository
from components.payment import service, exceptions

router = APIRouter(prefix="/payments", tags=["Payments"])


@router.post("/{order_id}")
async def make_payment(
    order_id: int,
    token: str,
    user=Depends(get_current_user),
    orders_repository: repository.OrdersRepository = Depends(
        repository.OrdersRepository.get
    ),
    payment_service: service.PaymentService = Depends(service.PaymentService.get),
):
    # Validate an order
    db_order = await orders_repository.get_order(order_id)
    if not db_order:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Order not found"
        )
    if db_order.customer_id != user["id"]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Not authorized to make payment for this order",
        )

    try:
        await payment_service.make_payment(token, db_order)
    except exceptions.PaymentError:
        # Log message here
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Card error"
        )
    except exceptions.PaymentServiceError:
        # Log message here
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Payment service error"
        )

import fastapi
from fastapi import APIRouter, Depends

from components.customer.repository import CustomersRepository
from components.customer.schemas import CustomerCreate, CustomerUpdate, CustomerItem

router = APIRouter(prefix="/customers", tags=["Customers"])


@router.post(
    "/", response_model=CustomerItem, status_code=fastapi.status.HTTP_201_CREATED
)
async def create_customer(
    customer: CustomerCreate,
    customers_repo: CustomersRepository = Depends(CustomersRepository.get),
):
    db_customer = await customers_repo.create_customer(customer)
    return CustomerItem.from_orm(db_customer)


@router.get("/", response_model=list[CustomerItem])
async def get_customers(
    customers_repo: CustomersRepository = Depends(CustomersRepository.get),
):
    customers = await customers_repo.get_customers()
    return list(map(lambda x: CustomerItem.from_orm(x), customers))


@router.get("/{email}", response_model=CustomerItem)
async def get_customer_by_email(
    email: str, customers_repo: CustomersRepository = Depends(CustomersRepository.get)
):
    db_customer = await customers_repo.get_customer_by_email(email)
    if db_customer is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="Customer not found"
        )
    return CustomerItem.from_orm(db_customer)


@router.put("/{id}", response_model=CustomerItem)
async def update_customer(
    id: int,
    customer: CustomerUpdate,
    customers_repo: CustomersRepository = Depends(CustomersRepository.get),
):
    db_customer = await customers_repo.update_customer(id, customer)
    if db_customer is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="Customer not found"
        )
    return CustomerItem.from_orm(db_customer)


@router.delete("/{id}", status_code=fastapi.status.HTTP_204_NO_CONTENT)
async def delete_customer(
    id: int,
    customers_repo: CustomersRepository = Depends(CustomersRepository.get),
):
    db_customer = await customers_repo.delete_customer(id)
    if db_customer is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="Customer not found"
        )
    return CustomerItem.from_orm(db_customer)

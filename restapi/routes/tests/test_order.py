from datetime import datetime
import pytest
from fastapi.testclient import TestClient
from fastapi import FastAPI
from components.order.models import Order, OrderItem
from components.core import database
from restapi.routes.order import router as orders_router
from components.order.repository import OrdersRepository

app = FastAPI()
app.dependency_overrides[database.get] = lambda: None
app.include_router(orders_router)

client = TestClient(app)


@pytest.fixture
def order_create():
    return {"customer_id": 1, "total": 100.0}


@pytest.fixture
def order_item_create():
    return {"order_id": 1, "product_id": 1, "quantity": 2, "price": 50.0}


@pytest.fixture
def order_update():
    return {"status": "completed", "total": 120.0}


@pytest.fixture
def order_item_update():
    return {"quantity": 3, "price": 60.0}


def test_create_order(mocker, order_create):
    mocker.patch.object(
        OrdersRepository,
        "create_order",
        return_value=Order(
            **order_create,
            status="PENDING",
            order_date=datetime.now().isoformat(),
            id=1,
        ),
    )
    response = client.post("/orders", json=order_create)
    assert response.status_code == 201
    assert response.json()["id"] == 1
    assert response.json()["customer_id"] == order_create["customer_id"]
    assert response.json()["total"] == order_create["total"]


def test_create_order_item(mocker, order_item_create):
    mocker.patch.object(
        OrdersRepository,
        "create_order_item",
        return_value=OrderItem(**order_item_create, id=1),
    )
    response = client.post("/orders/items", json=order_item_create)
    assert response.status_code == 201
    assert response.json()["id"] == 1
    assert response.json()["order_id"] == order_item_create["order_id"]
    assert response.json()["product_id"] == order_item_create["product_id"]
    assert response.json()["quantity"] == order_item_create["quantity"]
    assert response.json()["price"] == order_item_create["price"]


def test_update_order(mocker, order_update):
    order_id = 1
    mocker.patch.object(
        OrdersRepository,
        "update_order",
        return_value=Order(
            **order_update,
            order_date=datetime.now().isoformat(),
            id=order_id,
            customer_id=1,
        ),
    )
    response = client.put(f"/orders/{order_id}", json=order_update)
    assert response.status_code == 200
    assert response.json()["id"] == order_id
    assert response.json()["status"] == order_update["status"]
    assert response.json()["total"] == order_update["total"]


def test_update_order_item(mocker, order_item_update):
    order_item_id = 1
    mocker.patch.object(
        OrdersRepository,
        "update_order_item",
        return_value=OrderItem(
            **order_item_update, id=order_item_id, order_id=1, product_id=1
        ),
    )
    response = client.put(f"/orders/items/{order_item_id}", json=order_item_update)
    assert response.status_code == 200
    assert response.json()["id"] == order_item_id
    assert response.json()["quantity"] == order_item_update["quantity"]
    assert response.json()["price"] == order_item_update["price"]


def test_get_orders(mocker):
    order_id = 1
    mocker.patch.object(
        OrdersRepository,
        "get_orders",
        return_value=[
            Order(
                id=order_id,
                customer_id=1,
                total=100.0,
                status="completed",
                order_date="2023-04-30",
            )
        ],
    )
    response = client.get("/orders")
    assert response.status_code == 200
    assert isinstance(response.json(), list)
    assert response.json()[0]["id"] == order_id
    assert response.json()[0]["customer_id"] == 1
    assert response.json()[0]["total"] == 100.0
    assert response.json()[0]["status"] == "completed"
    assert response.json()[0]["order_date"] == "2023-04-30"


def test_get_order(mocker):
    order_id = 1
    mocker.patch.object(
        OrdersRepository,
        "get_order",
        return_value=Order(
            id=order_id,
            customer_id=1,
            total=100.0,
            status="completed",
            order_date="2023-04-30",
        ),
    )
    response = client.get(f"/orders/{order_id}")
    assert response.status_code == 200
    assert response.json()["id"] == order_id
    assert response.json()["customer_id"] == 1
    assert response.json()["total"] == 100.0
    assert response.json()["status"] == "completed"
    assert response.json()["order_date"] == "2023-04-30"


def test_get_order_not_found(mocker):
    order_id = 999
    mocker.patch.object(OrdersRepository, "get_order", return_value=None)
    response = client.get(f"/orders/{order_id}")
    assert response.status_code == 404
    assert response.json()["detail"] == "Order not found"

import fastapi
from fastapi import APIRouter, Depends

from components.order.repository import OrdersRepository
from components.order.schemas import (
    OrderCreate,
    OrderItemCreate,
    OrderUpdate,
    OrderItemUpdate,
    OrderItemItem,
    OrderItemSchema,
)

router = APIRouter(prefix="/orders", tags=["Orders"])


@router.post(
    "/", response_model=OrderItemSchema, status_code=fastapi.status.HTTP_201_CREATED
)
async def create_order(
    order: OrderCreate, orders_repo: OrdersRepository = Depends(OrdersRepository.get)
):
    db_order = await orders_repo.create_order(order)
    return OrderItemSchema.from_orm(db_order)


@router.post(
    "/items", response_model=OrderItemItem, status_code=fastapi.status.HTTP_201_CREATED
)
async def create_order_item(
    order_item: OrderItemCreate,
    orders_repo: OrdersRepository = Depends(OrdersRepository.get),
):
    db_order_item = await orders_repo.create_order_item(order_item)
    return OrderItemItem.from_orm(db_order_item)


@router.put("/{order_id}", response_model=OrderItemSchema)
async def update_order(
    order_id: int,
    order: OrderUpdate,
    orders_repo: OrdersRepository = Depends(OrdersRepository.get),
):
    db_order = await orders_repo.update_order(order_id, order)
    if db_order is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="Order not found"
        )
    return OrderItemSchema.from_orm(db_order)


@router.put("/items/{order_item_id}", response_model=OrderItemItem)
async def update_order_item(
    order_item_id: int,
    order_item: OrderItemUpdate,
    orders_repo: OrdersRepository = Depends(OrdersRepository.get),
):
    db_order_item = await orders_repo.update_order_item(order_item_id, order_item)
    if db_order_item is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="Order item not found"
        )
    return OrderItemItem.from_orm(db_order_item)


@router.get("/", response_model=list[OrderItemSchema])
async def get_orders(orders_repo: OrdersRepository = Depends(OrdersRepository.get)):
    orders = await orders_repo.get_orders()
    return list(map(lambda x: OrderItemSchema.from_orm(x), orders))


@router.get("/{order_id}", response_model=OrderItemSchema)
async def get_order(
    order_id: int, orders_repo: OrdersRepository = Depends(OrdersRepository.get)
):
    db_order = await orders_repo.get_order(order_id)
    if db_order is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="Order not found"
        )
    return OrderItemSchema.from_orm(db_order)

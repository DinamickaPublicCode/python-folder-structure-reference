import fastapi
from fastapi import APIRouter, Depends

from components.inventory.schemas import InventoryCreate, InventoryUpdate, InventoryItem
from components.inventory.repository import InventoryRepository

router = APIRouter(prefix="/inventory", tags=["Inventory"])


@router.post(
    "/", response_model=InventoryItem, status_code=fastapi.status.HTTP_201_CREATED
)
async def create_inventory(
    inventory: InventoryCreate,
    inventory_repo: InventoryRepository = Depends(InventoryRepository.get),
):
    db_inventory = await inventory_repo.create_inventory(inventory)
    return InventoryItem.from_orm(db_inventory)


@router.put("/{inventory_id}", response_model=InventoryItem)
async def update_inventory(
    inventory_id: int,
    inventory: InventoryUpdate,
    inventory_repo: InventoryRepository = Depends(InventoryRepository.get),
):
    db_inventory = await inventory_repo.update_inventory(inventory_id, inventory)
    if db_inventory is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="Inventory not found"
        )
    return InventoryItem.from_orm(db_inventory)


@router.get("/", response_model=list[InventoryItem])
async def get_inventories(
    inventory_repo: InventoryRepository = Depends(InventoryRepository.get),
):
    inventories = await inventory_repo.get_inventories()
    return list(map(lambda x: InventoryItem.from_orm(x), inventories))


@router.get("/{inventory_id}", response_model=InventoryItem)
async def get_inventory_by_id(
    inventory_id: int,
    inventory_repo: InventoryRepository = Depends(InventoryRepository.get),
):
    db_inventory = await inventory_repo.get_inventory_by_id(inventory_id)
    if db_inventory is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="Inventory not found"
        )
    return InventoryItem.from_orm(db_inventory)


@router.get("/product/{product_id}", response_model=InventoryItem)
async def get_inventory_by_product_id(
    product_id: int,
    inventory_repo: InventoryRepository = Depends(InventoryRepository.get),
):
    db_inventory = await inventory_repo.get_inventory_by_product_id(product_id)

    if db_inventory is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="Inventory not found"
        )
    return InventoryItem.from_orm(db_inventory)

import fastapi
from fastapi import APIRouter, Depends

from components.shipping.repository import ShippingRepository
from components.shipping.schemas import ShippingCreate, ShippingUpdate, ShippingItem

router = APIRouter(prefix="/shipping", tags=["Shipping"])


@router.post(
    "/", response_model=ShippingItem, status_code=fastapi.status.HTTP_201_CREATED
)
async def create_shipment(
    shipping: ShippingCreate,
    shipping_repo: ShippingRepository = Depends(ShippingRepository.get),
):
    db_shipping = await shipping_repo.create_shipment(shipping)
    return ShippingItem.from_orm(db_shipping)


@router.put("/{shipping_id}", response_model=ShippingItem)
async def update_shipment(
    shipping_id: int,
    shipping: ShippingUpdate,
    shipping_repo: ShippingRepository = Depends(ShippingRepository.get),
):
    db_shipping = await shipping_repo.update_shipment(shipping_id, shipping)
    if db_shipping is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="Shipment not found"
        )
    return ShippingItem.from_orm(db_shipping)


@router.get("/{shipping_id}", response_model=ShippingItem)
async def get_shipment(
    shipping_id: int,
    shipping_repo: ShippingRepository = Depends(ShippingRepository.get),
):
    db_shipping = await shipping_repo.get_shipment(shipping_id)

    if db_shipping is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="Shipment not found"
        )
    return ShippingItem.from_orm(db_shipping)


@router.get("/order/{order_id}", response_model=list[ShippingItem])
async def get_shipments_for_order(
    order_id: int, shipping_repo: ShippingRepository = Depends(ShippingRepository.get)
):
    shipments = await shipping_repo.get_shipments_for_order(order_id)
    return list(map(lambda x: ShippingItem.from_orm(x), shipments))

from typing import Annotated

import fastapi
from fastapi import APIRouter
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm

from components.security import utils
from components.user import models
from components.user import repository
from components.user import schemas
from components.user.service import AuthService
from restapi import security

router = APIRouter(prefix="/users", tags=["Users"])


@router.post("/token", response_model=schemas.Token)
async def login_for_access_token(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
    auth_service: AuthService = Depends(AuthService.get),
):
    user = await auth_service.authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    access_token = utils.create_access_token(data={"sub": user.email})

    return {"access_token": access_token, "token_type": "bearer"}


@router.get("/users/me", response_model=schemas.UserFull)
async def read_users_me(
    current_user: Annotated[models.User, Depends(security.get_current_user)]
):
    return schemas.UserFull.from_orm(current_user)


@router.post("/signup", response_model=schemas.UserSchema)
async def create_user(
    user: schemas.UserCreate,
    user_repo: repository.UserRepository = fastapi.Depends(
        repository.UserRepository.get
    ),
):
    db_user = await user_repo.create_user(models.User(**user.dict()))

    created_user = schemas.UserSchema.from_orm(db_user)
    return created_user


@router.put(
    "/{user_id}",
    response_model=schemas.UserFull,
    dependencies=[Depends(security.requires_permission("manage_users"))],
)
async def update_user(
    user_id: int,
    user: schemas.UserUpdate,
    user_repo: repository.UserRepository = fastapi.Depends(
        repository.UserRepository.get
    ),
):
    db_user = await user_repo.update_user_by_id(user_id, user.dict())
    if not db_user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not found"
        )
    return schemas.UserFull.from_orm(db_user)


@router.put(
    "/{user_id}/permissions/add",
    dependencies=[Depends(security.requires_permission("manage_users"))],
)
async def add_user_permission(
    user_id: int,
    permission: schemas.UserPermissionUpdate,
    users_repo: repository.UserRepository = Depends(repository.UserRepository.get),
):
    db_user = await users_repo.get_user_by_id(user_id)
    if db_user is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="User not found"
        )

    permission_obj = await users_repo.get_permission_by_name(permission.name)
    if permission_obj is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="Permission not found"
        )

    db_user.permissions.append(permission_obj)
    await users_repo.update_user(db_user)

    return schemas.UserFull.from_orm(db_user)


@router.get(
    "/{user_id}",
    response_model=schemas.UserFull,
    dependencies=[Depends(security.requires_permission("manage_users"))],
)
async def get_user_by_id(
    user_id: int,
    users_repo: repository.UserRepository = Depends(repository.UserRepository.get),
):
    db_user = await users_repo.get_user_by_id(user_id)
    if db_user is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="User not found"
        )
    return schemas.UserFull.from_orm(db_user)


@router.get(
    "/{user_id}/roles",
    response_model=list[schemas.RoleItem],
    dependencies=[Depends(security.requires_permission("manage_users"))],
)
async def get_user_roles(
    user_id: int,
    users_repo: repository.UserRepository = Depends(repository.UserRepository.get),
):
    db_user = await users_repo.get_user_by_id(user_id)
    if db_user is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="User not found"
        )
    return [schemas.RoleItem.from_orm(x) for x in db_user.roles]
